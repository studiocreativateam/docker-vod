# VOD

## Setup
1. Clone repository by ssh `git clone git@bitbucket.org:studiocreativateam/vod.git` or by https `git clone https://[username]@bitbucket.org/studiocreativateam/vod.git`
2. Copy .env.example `cp .env.example .env`
3. Copy docker/nginx/conf.d/app.conf.example and change server ip if you need `cp docker/nginx/conf.d/app.conf.example docker/nginx/conf.d/app.conf`
4. Copy php configuration `cp docker/local.ini.example docker/local.ini`
5. Copy configuration `cp docker/www.conf.example docker/www.conf`
6. Copy vod nginx `cp docker/vod/nginx/conf.d/app.conf.example docker/vod/nginx/conf.d/app.conf`
7. Run command `make up` or `make up-win`
