<!DOCTYPE html>
<html>
<head>
    <meta charset=utf-8/>
    <title>V[i]O[ł]D[i]</title>
    <link href="https://vjs.zencdn.net/7.20.3/video-js.css" rel="stylesheet" />
    <style>
        body {
            display: flex;
            height: 100vh;
            padding: 0;
            margin: 0;
        }

        #video_index {
            margin: auto;
        }
    </style>
</head>
<body>
<video-js id="video_index" width=800 height=600 class="vjs-default-skin" controls webkit-playsinline="true" playsinline="true">
    <source src="http://localhost:3030/hls/devito,360p.mp4,480p.mp4,720p.mp4,.en_US.vtt,.urlset/master.m3u8"
            type="application/x-mpegURL">
</video-js>

<script src="/js/main.js"></script>
</body>
</html>
