include .env

update:
	git pull
	docker exec -ti ${CONTAINER_NAME_APP} sh -c "npm install;npm run build"
up:
	docker-compose --env-file .env up -d
	docker exec -ti ${CONTAINER_NAME_APP} sh -c "npm install;npm run build"
down:
	docker-compose down
bash:
	docker exec -it ${CONTAINER_NAME_APP} /bin/bash
